const config = {
    db:{
        host:process.env.HOST,
        user:process.env.USER,
        password:'',
        database:process.env.DATABASE,
    },
    listPerPage: 10,
};

module.exports = config;