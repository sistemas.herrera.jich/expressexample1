const express =require('express');
const router = express.Router();

const usuariosController= require('../controller/usuario.controller');

router.get('/getall', usuariosController.getAll);
router.get('/getById', usuariosController.getById);
router.post('/create', usuariosController.insert);
router.put('/update/:id', usuariosController.update);

module.exports = router;

