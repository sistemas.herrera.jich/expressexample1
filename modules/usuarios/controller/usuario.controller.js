const usuariosService = require('../services/usuarios.service');

async function getAll(req, res) {
    try {
        const numPage = req.body.numPage;
        const data = await usuariosService.getAll(numPage);

        res.status(200).json({
            idValid: true,
            message: 'Listado de Usuarios',
            data: data
        });
    } catch (error) {
        res.status(500).json({
            isValid: false,
            message: error,
        });
    }
}

async function getById(req, res){
    try {
        const id = req.body.id;
        const data = await usuariosService.getById(id);

        res.status(200).json({
            isValid: true,
            message: 'Usuario encontrado',
            data: data
        })
    } catch (error) {
        res.status(500).json({
            isValid: false,
            message: 'No se encontro al usuario'
        });
    }
}

async function insert(req, res){
    try {
        const data = req.body;
        const result = await usuariosService.insert(data);

        res.status(200).json({
            isValid: true,
            message: 'Usuario agregado correctamete',
            data: result,
        });
    } catch (error) {
        res.status(500).json({
            isValid: false,
            message: 'No se agrego al usuario'
        });
    }
}

async function update(req, res) {
    try {
        const id = req.params.id;
        const dtoUsuario = req.body;
        const result = await usuariosService.update(dtoUsuario,id);

        res.status(200).json({
            isValid:true,
            message:'Actulizado correctamentee',
            data: result
        });
    } catch (error) {
        res.status(500).json({
            isValid: false,
            message: 'No se actulizo al usuario'
        });
    }
}

module.exports = {
    getAll,
    getById,
    insert,
    update
}