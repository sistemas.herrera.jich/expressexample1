const db = require('../../../db/connect.db');
const config = require('../../../db/config.db');
const helper = require('../../../db/helper.db');

//getAll
async function getAll(page=1) {
    try {
        const offset = helper.getOffset(page, config.listPerPage);
        const rows = await db.query(`SELECT * FROM usuarios LIMIT ${offset}, ${config.listPerPage}`);
        const data = helper.emptyOrRows(rows);
        const meta = {page};

        return {
            data,
            meta
        }
    } catch (error) {
        throw new Error(error);
    }
}
//getById
async function getById(id){
    try {
        const rows = await db.query(`SELECT * FROM usuarios WHERE id=${id}`);
        const data = helper.emptyOrRows(rows);
        return data;
    } catch (error) {
        throw new Error(error);
    }
}
//Insert
async function insert(data){
    try {
        const username = data.username;
        const password = data.password;
        const id_empleado = data.id_empleado;

        const result = await db.query(`INSERT INTO usuarios(username, password, id_empleado) VALUES('${username}', '${password}', ${id_empleado})`);

        return result;
    } catch (error) {
        throw new Error(error);
    }
}
//Update
async function update(dtoUsuarios, id) {
    try {
        const username = dtoUsuarios.username;
        const password = dtoUsuarios.password;
        const id_empleado = dtoUsuarios.id_empleado;

        const result = await db.query(`UPDATE usuarios SET username='${username}', password='${password}' WHERE id=${id}`);

        return result;

    } catch (error) {
        throw new Error(error);
    }
}

module.exports = {
    getAll,
    getById,
    insert,
    update
}