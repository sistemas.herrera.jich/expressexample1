/**
 * Servidor Node JS con Express Ejecutando en puerto 3005
 */
const express = require('express');
const cors = require('cors');
require('dotenv').config();
const routes = require('./routes');

const app = express();
app.use(cors());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({extended: true, limit: '50mb'}));

async function getDatos(req, res) {
    try {
        const email = req.body.email;
        const pdw = req.body.pwd;

        if (!email) {
            return res.json({
                isValid: false,
                message: 'No existe el email',
                data: 'Adios universo'
            });
        }

        res.json({
            isValid: true,
            message: 'Estas en el get principal',
            data: `'Hola universo' ${email} ${pdw}`
        });

    } catch (error) {
        console.log(error);
        res.json({
            isValid: false,
            message: 'Estas en el error principal',
            data: 'Adios universo'
        });
    }
}

// primer get el servidor nos habla
app.get('/', (req, res) => {
    getDatos(req, res)
});

app.use("/usuarios", routes.usuarios);

const server = require('http').createServer(app);

const port = process.env.PORT;

server.listen(port, (err) => {
    if (err) {
        throw new Error(err);
    }

    console.log(`Server ejecutando en http://localhost:${port}`);
});
